package com.vortex.smartlite;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.vortex.smartlite.Login.LoginActivity;
import com.vortex.smartlite.Login.SaveRemember;
import com.vortex.smartlite.io.ApiAdapter;
import com.vortex.smartlite.models.Send;
import com.vortex.smartlite.services.JobIntentService;
import com.vortex.smartlite.services.WebSocketService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private TextView usename;
    private TextView alert;
    private Button safe;
    private ImageButton logout;
    private ImageButton link;
    private int sig;
    Dialog customDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        usename = findViewById(R.id.username);

        if (!SaveRemember.get(this).isLoggedIn()) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }else{
            SharedPreferences preferences = getSharedPreferences("SAVE_NAME",Context.MODE_PRIVATE);
            String name = preferences.getString("SAVE_USERNAME","x");
            usename.setText(name);
            int signal = preferences.getInt("ID_DISP",0);
            sig = signal;
        }

        alert = findViewById(R.id.Alerta);
        alert.setText(R.string.Alert);
        alert.setTextColor(getResources().getColor(R.color.red));

        safe = findViewById(R.id.Safe);
        safe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendSignal();
            }
        });

        logout = findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveRemember.get(MainActivity.this).logOut();
                startActivity(new Intent(MainActivity.this,LoginActivity.class));
                finish();
            }
        });


        link = findViewById(R.id.linked);
        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://www.vortexrobotic.com";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent intent = new Intent(this, JobIntentService.class);
            startService(intent);
        } else {
            Intent local = new Intent(this, WebSocketService.class);
            startService(local);
        }
    }

    private void Dialog(String Content, String title) {
        customDialog = new Dialog(this, R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.dialog_design);

        TextView titulo = (TextView) customDialog.findViewById(R.id.titulo);
        titulo.setText(title);

        TextView contenido = (TextView) customDialog.findViewById(R.id.contenido);
        contenido.setText(Content);

        ((Button) customDialog.findViewById(R.id.aceptar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }

    public void onBackPressed(){
        Dialog(getString(R.string.exit),getString(R.string.question));
    }

    private void SendSignal() {
        int signal = 0;
        Call<Send> data = ApiAdapter.getApiService().Send_alert(sig,signal,signal);
        data.enqueue(new Callback<Send>() {
            @Override
            public void onResponse(Call<Send> call, Response<Send> response) {
                Dialog(getString(R.string.content),getString(R.string.title));
            }

            @Override
            public void onFailure(Call<Send> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"No se ha podido establecer conexion con el servidor",
                        Toast.LENGTH_LONG).show();

            }
        });
    }
}
