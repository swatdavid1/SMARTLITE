package com.vortex.smartlite.Login;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.vortex.smartlite.models.Responsejson;

public class SaveRemember {

    private final SharedPreferences sharedPreferences;

    private boolean mIsLoggedIn = false;

    private static SaveRemember INSTANCE;

    public static SaveRemember get(Context context){
        if (INSTANCE == null){
            INSTANCE = new SaveRemember(context);
        }
        return INSTANCE;
    }

    private SaveRemember(Context context) {
        sharedPreferences = context.getApplicationContext()
                .getSharedPreferences("SAVE_NAME", Context.MODE_PRIVATE);

        mIsLoggedIn = !TextUtils.isEmpty(sharedPreferences.getString("SAVE_SMART", null));
    }

    public boolean isLoggedIn(){
        return mIsLoggedIn;
    }

    public void saveUser(Responsejson remember, int i, int j){
        if(remember != null){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("SAVE_USERNAME", remember.getUsers().get(i).getUsername());
            editor.putString("SAVE_SMART", remember.getUsers().get(i).getDispSmart().get(j).getNumeroSerial());
            editor.putString("SAVE_EDE",remember.getUsers().get(i).getDispSmart().get(j).getNumeroSerialE());
            editor.putInt("ID_DISP",remember.getUsers().get(i).getDispSmart().get(j).getId());
            editor.apply();

            mIsLoggedIn = true;
        }
    }


    public void logOut(){
        mIsLoggedIn = false;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("SAVE_USERNAME", null);
        editor.putString("SAVE_SMART", null);
        editor.putString("SAVE_EDE", null);
        editor.putInt("ID_DISP", 0);
        editor.apply();
    }
}
